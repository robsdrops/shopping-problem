/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package internetshopping;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;


import net.sourceforge.yamlbeans.YamlException;
import net.sourceforge.yamlbeans.YamlReader;

/**
 *
 * @author robert
 */

public class ReadYAML {

  String filePath = "";
  
  //shop_name -> products_list -> product_name, price, amount, delivery_cost 
  Map<String, Map<String, ArrayList<Map<String, String>>>> parsedData;

  public ReadYAML(String filePath){
	  this.filePath = filePath;
  }

  void run(){
	  FileReader file_reader = null;
	try {
		file_reader = new FileReader(this.filePath);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	  YamlReader reader = new YamlReader(file_reader);
	  Object object = null;
	try {
		object = reader.read();
	} catch (YamlException e) {
		e.printStackTrace();
	}
	parsedData = (Map<String, Map<String, ArrayList<Map<String, String>>>>)object;
  }

  Map getParsedData(){
	  return parsedData;
  }
 }