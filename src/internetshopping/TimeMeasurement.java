/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package internetshopping;

/**
 *
 * @author robert
 */
public class TimeMeasurement {
    long start;
    long end;
    
    void start(){
        start = System.currentTimeMillis();
    }
    
    void end(){
        end = System.currentTimeMillis();
    }
    
    long timeMeasured(){
        return end - start;
    }
}
