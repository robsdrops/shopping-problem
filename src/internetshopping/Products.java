package internetshopping;

/**
 *
 * @author Konrad Dzień
 */
public class Products {

    String name;
    int price;
    int deliveryCost;
    int amount;

    public Products(String name, int price, int deliveryCost, int amount){
        this.name = name;
        this.price = price;
        this.deliveryCost = deliveryCost;
        this.amount = amount;
    }

    int getPrice(){
        return price;
    }

    String getName(){
        return name;
    }

    int getDeliveryCost(){
        return deliveryCost;
    }
    
    int getAmount(){
        return amount;
    }

}
