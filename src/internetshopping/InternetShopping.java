
package internetshopping;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Konrad Dzień
 */
public class InternetShopping {

    public static void main(String[] args) {

        int ileMam=150;
        
        ArrayList<Shop> shops = new ArrayList();
        ReadYAML shopsReader = new ReadYAML("src/internetshopping/shops.yml");
        shopsReader.run();
        ShopsLoader shopsLoader = new ShopsLoader(shopsReader.getParsedData());
        shopsLoader.load();
        shops = shopsLoader.getShops();
        
        ArrayList<Products> shoppingBasket = new ArrayList();
        ReadYAML basketReader = new ReadYAML("src/internetshopping/shopping_basket.yml");
        basketReader.run();
        ShoppingBasketLoader basketLoader = new ShoppingBasketLoader(basketReader.getParsedData());
        basketLoader.load();
        shoppingBasket = basketLoader.getProducts();
        
        
        
        //====================problem decyzyjny=======================
        // zbieranie produktów
        String myProducts[] = new String[shoppingBasket.size()];
        int amountMyProducts[] = new int[shoppingBasket.size()];
        for(int i=0; i<=shoppingBasket.size()-1;i++){
           myProducts[i] = shoppingBasket.get(i).name;
           amountMyProducts[i] = shoppingBasket.get(i).amount;
        }
        //wyświetlanie koszyka
        System.out.println("Twój koszyk:");
        for(int i=0; i<=myProducts.length-1;i++){
           System.out.print(myProducts[i] + " ilość: " + amountMyProducts[i] +","); 
        }
        System.out.println("");
        System.out.println("");
        
        //porownywanie ze sklepami
        for(int j=0; j<=shops.size()-1;j++){
            System.out.println("W sklepie " + shops.get(j).name + " znajdują się następujące produkty: ");
            for(int i=0; i<=shops.get(j).products.size()-1;i++){
                
                for(int k=0; k<=myProducts.length-1;k++){
                       if(myProducts[k].toString().equals(shops.get(j).products.get(i).name)){
                       System.out.println(shops.get(j).products.get(i).name + " w ilości " + shops.get(j).products.get(i).amount + " w cenie za sztuke "+ shops.get(j).products.get(i).price + " i koszcie dostawy " + shops.get(j).products.get(i).deliveryCost);
                       }
                }
                
            }
            System.out.println(" ");
        }
        //============================================================
    }
}
