/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package internetshopping;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author robert
 */
public class ShoppingBasketLoader {
    ArrayList<Products> products = null;
    Map<String, ArrayList<Map<String, String>>> mappedProducts = null;
    
    public ShoppingBasketLoader(Map data){
        mappedProducts = data;
        products = new ArrayList();
    }
    
    void load(){
        for (Map.Entry<String, ArrayList<Map<String, String>>>
            entry : mappedProducts.entrySet()){

            ArrayList<Map<String, String>>
                 products_list = entry.getValue();

            for (Map<String, String> product : products_list){
                String product_name =
                        product.get("name");

                int product_amount =
                        Integer.parseInt(product.get("amount"));

                Products new_products =
                        new Products(product_name,
                                -1,
                                -1,
                                product_amount);

                products.add(new_products);
            }
        }
    }
    
    ArrayList getProducts(){
        return products;
    }
}
