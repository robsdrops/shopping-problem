
package internetshopping;
import java.util.ArrayList;

/**
 *
 * @author Konrad Dzień
 */
public class Shop {

    String name;
    ArrayList<Products> products;
    int totalPrice = 0;

    public Shop(String name, ArrayList<Products> products){
        this.name = name;
        this.products = products;
    }

    public ArrayList<Products> getProducts(){
        return products;
    }

    void canIBuyAllThingsInThisShop(int howMuchGoldCanIPay){

        for(Products product : products){
           totalPrice = totalPrice + product.getPrice() + product.getDeliveryCost();
        }
        if(howMuchGoldCanIPay >= totalPrice){
            System.out.println("Tak, możesz kupić rzeczy w sklepie o nazwie " + name);
        }
        else{
            System.out.println("Nie możesz kupić wszystkich produktów ze sklepu " + name);
        }
    }

    @Override
    public String toString(){
        String stringedObject = "";
        for(Products product : products){
            stringedObject += "Id produktu: " + product.getName() + " Cena produktu: " + product.getPrice() +"\n";
        }
        return stringedObject;
    }
}
