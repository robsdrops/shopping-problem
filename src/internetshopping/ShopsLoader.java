package internetshopping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ShopsLoader {
	Map<String, Map<String, ArrayList<Map<String, String>>>> mappedShops = null;
        ArrayList<Shop> shops = null;
        
	public ShopsLoader(Map data){
		mappedShops = data;
                shops = new ArrayList();
	}

	void load(){            
            for (Map.Entry<String, Map<String, ArrayList<Map<String, String>>>>
                    entry : mappedShops.entrySet()){
                
                String shop_name = entry.getKey();
                
                Map<String, ArrayList<Map<String, String>>>
                        products = entry.getValue();
                
                ArrayList<Map<String, String>>
                        products_list_mapped = products.get("products");
                
                ArrayList<Products> products_list = new ArrayList();
                
                for (Map<String, String> product : products_list_mapped){
                    String product_name =
                            product.get("name");
                    
                    int product_price =
                            Integer.parseInt(product.get("price"));
                    
                    int product_delivery_cost =
                            Integer.parseInt(product.get("delivery"));
                    
                    int product_amount =
                            Integer.parseInt(product.get("amount"));
                    
                    Products new_products =
                            new Products(product_name,
                                    product_price,
                                    product_delivery_cost,
                                    product_amount);
                    
                    products_list.add(new_products);
                }
                Shop shop = new Shop(shop_name, products_list);
                shops.add(shop);
            }
	}
        
        ArrayList<Shop> getShops(){
            return shops;
        }
}
